const express = require('express');
const router = express.Router();
const axios = require('axios');
const { transferencia } = require('../modulos');
const transfHandlers = transferencia({ axios });

// router.get('/', 
//    nuevoDestinatarioController.nuevoDestinatarioController
// );


router.post('/', 
   transfHandlers.agregar
);

module.exports = router;