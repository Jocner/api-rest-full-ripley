const mongoose = require('mongoose');

const HistorialsSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    rut: {
        type: String,
        required: true,
        trim: true
    },
    banco: {
        type: String,
        required: true,
        trim: true
    },
    tipocuenta: {
        type: String,
        require: true,
        trim: true
    },
    monto: {
        type: String,
        require: true,
        trim: true
    },
    registro: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Historial', HistorialsSchema);