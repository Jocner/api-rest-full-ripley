const express = require('express');
const conectarDB = require('./config/db');
const cors = require('cors');
const axios = require('axios');
const app = express();
//const agregar = require('./modules/transferencia')
// const { transferencia } = require('../src/modulos');

// const transfHandlers = transferencia({ axios });


conectarDB();

app.use(cors());

app.use( express.json({ extended: true}));

const port = process.env.PORT || 4000;


 app.use('/api/banco', require('./router/bancos'));
 app.use('/api/registro', require('./router/registro'));
 app.use('/api/busqueda', require('./router/busqueda'));
 app.use('/api/transferir', require('./router/transacion'));
 app.use('/api/historial', require('./router/historial'));


app.listen(port , () => {
    console.log(`El servidor esta funcionando en el puerto ${port}`);
});
